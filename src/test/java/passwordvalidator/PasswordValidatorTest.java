package passwordvalidator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import password.PasswordValidator;

/**
 *
 * @author mitpa
 */
public class PasswordValidatorTest {
    

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    //checkDigit 
    
    public void testIsValidPasswordDigitRegular() {
        System.out.println("isValidPassword");
        String userInput = "abc0dddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testthrowsPasswordInValidDigitRegular() {
        System.out.println("isValidPassword");
        String userInput = "abcedddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryInValidPasswordDigitRegular() {
        System.out.println("isValidPassword");
        String userInput = "abc100dAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryOutValidPasswordDigitRegular() {
        System.out.println("isValidPassword");
        String userInput = "1bcedddA$9";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    
    //  checkCapital  
   public void testIsValidPasswordCheckCapital() {
        System.out.println("isValidPassword");
        String userInput = "AbC0dddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testthrowsInValidPasswordCheckCapital() {
        System.out.println("isValidPassword");
        String userInput = "abcedddak&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryInValidPasswordCheckCapital() {
        System.out.println("isValidPassword");
        String userInput = "abC100dAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryOutValidPasswordCheckCapital() {
        System.out.println("isValidPassword");
        String userInput = "Abced9dA$V";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    //checkSpecChar  
    public void testIsValidPasswordCheckSpecChar() {
        System.out.println("isValidPassword");
        String userInput = "AbC0dddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testthrowsInValidPasswordCheckSpecChar() {
        System.out.println("isValidPassword");
        String userInput = "abcedddak0";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryInValidPasswordCheckSpecChar() {
        System.out.println("isValidPassword");
        String userInput = "abC&@0dAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryOutValidPasswordCheckSpecChar() {
        System.out.println("isValidPassword");
        String userInput = "$bced9dA$&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    //checkMinLength
    public void testIsValidPasswordCheckMinLength() {
        System.out.println("isValidPassword");
        String userInput = "AbC0dddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testthrowsInValidPasswordCheckMinLength() {
        System.out.println("isValidword");
        String userInput = "abcedddak0";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryInValidPasswordCheckMinLength() {
        System.out.println("isValidPassword");
        String userInput = "abC&@0dAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    public void testBoundaryOutValidPasswordCheckMinLength() {
        System.out.println("isValidPassword");
        String userInput = "$bced9dA$&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    
}
